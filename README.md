# query_metabarcoding_data

a shiny application to query information about our metabarcoding data


Import the database using postgresql

```
psql -U peguerin chorizo < db/chorizo.pgsql
```

R packages

```
requiredPackages = c("shinycssloaders",
                     "shinydashboard",
                     "shiny",
                     "RPostgreSQL",
                     "DT",
                     "dplyr",
                     "pool",
                     "yaml")

requiredPackagesNew <- requiredPackages[!(requiredPackages %in% installed.packages()[, "Package"])]
install.packages(requiredPackagesNew)
```


Inside your R session, load shiny and then, run the web-app in local

```
## load shiny library
library('shiny')
## define web browser to use
options(browser="/usr/bin/firefox")
## run shiny web app
runApp(launch.browser=TRUE)
```

